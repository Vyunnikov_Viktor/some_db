from datetime import datetime

from consumer_app.db import Entity

# Аналог нашего консьюмера, который берет из базы новые сообщения отправляет их и меняет статус
entities = Entity.select().where(Entity.status == 'NEW').limit(2).execute()
entity_ids = [entity.id for entity in entities]

Entity.update(status='SENT', send_time=datetime.now()).where(Entity.id << entity_ids).execute()
