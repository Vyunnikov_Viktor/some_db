from peewee import SqliteDatabase, Model, CharField, DateTimeField

db = SqliteDatabase('db.sqlite3')


class Entity(Model):
    payload = CharField()
    status = CharField()
    send_time = DateTimeField(null=True)

    class Meta:
        database = db
