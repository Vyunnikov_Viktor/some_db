from peewee import SqliteDatabase, Model, CharField, DateTimeField

db = SqliteDatabase('db.sqlite3')


class Entity(Model):
    payload = CharField()
    status = CharField()
    create_time = DateTimeField()

    class Meta:
        database = db
